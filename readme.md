# Lil Demo For Using Docker Volume to Pass A Secret To A Container

A volume is a directory that can be shared from a container and its host and visa versa. It can also be used to share data between multiple containers. When running an image in a container, you can mount a volume using the -v flag like so. Note that what follows -v is defining a 'bind mount' and you can think of it as hostdir:containerdir. 'alpine' is not part of the volume argument, it's the name of the image that's being used for the container.

```bash
docker run -it --name cool_container -v /c/shared:/data alpine
```

If you happen to be using windows or mac, this is not as straightforward as it would be in linux. This example is what I did to get this to work on 'boot2docker' on windows since this machine doesn't have hyper-v.

In order for the above command to work, you must first set a shared folder in virtualbox in /c/shared - the default shared folder is /c/Users and there's a few reasons why you wouldn't want a shared folder to be there - like if you want for the volumes to be available to other people signing into the computer.

To add a shared folder to boot2docker, open virtualbox and stop the virtual machines, which is called 'default'.
click settings -> shared folders -> add

The 'name' you specify will become a folder in the docker vm and it should be c/foldername - the path is the folder on your harddrive you are binding to it - browse to your desired folder and save the settings. Again - I'll highlight it again since it's non-intuitive - the name field will be turned into a directory in the virtual machine.

The next time you start the virtual machine, the folder will be created and you should see it in the filesystem of boot2docker in /c/foldername. When I say 'see it', I'm assuming that when you boot the virtual machine you see a graphical interface as I do - I've read that it's possible to ssh to the docker-machine but typing commands into the graphical interface accomplishes the same thing.

Now you can expose that folder's location in the boot2docker virtualmachine to a container when you create it using the command above.

Another way to create volumes is by using the 'docker volume create [volumename]' command - this is much better for reusing and sharing the same volume between multiple containers. These are called 'named volumes'. 

In order to use a named volume, a mountpoint will be created in the virtual machine filesystem - it's typically /mnt/sda1/var/lib/docker/volumes/[volumename]]/\_data. It doesn't seem like you can or need to change the mountpoint - apparently you can accomplish the same thing by using the 'device' field in the volume's 'options'. In order to bind the volume to the shared folder you set up in the virtualmachine, you must refer to it like //c/foldername (note the double backslash).

```bash
docker volume create --opt type=none --opt device=//c/supervol --opt o=bind supervol
docker inspect supervol
```

note - with docker for windows (as opposed to the virtualbox supported 'docker toolbox') the command is like so.

```bash
docker volume create --opt type=none --opt device=/c/redis_shared --opt o=bind redis_shared
```

inspect will return a json representation of the volume.

```bash
[
    {
        "CreatedAt": "2019-02-23T16:40:19Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/mnt/sda1/var/lib/docker/volumes/supervol/_data",
        "Name": "supervol",
        "Options": {
            "device": "//c/supervol",
            "o": "bind",
            "type": "none"
        },
        "Scope": "local"
    }
]
```

Now you can use the volume name in the -v command, which is 'supervol' in this example.


```bash
docker run -it --name cool_container -v supervol:/myvolumedir alpine
```

note - if you are trying to run a redis container with a named volume with port 6379 forwarded to the host machine, use the command below - note that -d means detached, so it will run in the background.

```bash
docker run --name my_redis -v redis_shared:/data -p 6379:6379 -d redis
```

You'll see that myvolumedir exists in the root of your container and files you add from windows will appear inside of the container and visa versa. In this repo I made it so the container's volume directory was called supervol as well, but realize it was confusing. Sorry about that.

When you commit the container and push the image, it will include the bindpoint, but it won't include the contents of the volume. It is your option when _running_ the image to attach a volume to it.

Below are the commands I used to get the image with the mountpoint pushed to the registry of this repo.

```bash
docker tag cool_image registry.gitlab.com/jojomickymack/volume01:my_tag
docker push registry.gitlab.com/jojomickymack/volume01:my_tag
```

What I'm actually trying to accomplish here is making it so I can pass a secret password to the docker container when it's run by a gitlab-runner. Take a look at the included 'config.toml' and look for the 'volumes' field under the 'runners.docker' section. In addition to the 'cache' directory which is included by default, I added 'supervol:/supervol'.

It assumes that the docker installation on the host machine has a volume called 'supervol' already set up - if it doesn't, there won't be any errors, but there won't be anything in the /supervol directory inside the container.

The idea is that if I put my secret password in the folder that is shared with the 'supervol' volume on the host system, I can access it in the container. If someone runs the image on a different computer, they'll have to set up a volume called 'supervol' and have a password in it as well.